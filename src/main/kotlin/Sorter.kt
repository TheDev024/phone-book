/**
 * Sorts given arrays by help of bubble sort algorithm.
 */
object Sorter {
    /**
     * Sorts given array of strings in increasing order according to given transformation with bubble sort algorithm.
     * @param array array to sort
     * @param transform transform to apply to each element
     */
    fun bubbleSort(array: Array<String>, transform: (String) -> String) {
        for (i in array.lastIndex downTo 1) for (j in 0 until i) if (transform(array[j]) > transform(array[j + 1])) swapElements(
            array, j, j + 1
        )
    }

    fun quickSort(array: Array<String>, left: Int = 0, right: Int = array.lastIndex, transform: (String) -> String) {
        if (right <= left) return

        val pivot = transform(array[right])
        var pIndex = right
        var i = left

        while (i != pIndex) if (transform(array[i++]) > pivot) {
            for (j in i - 1 until right) swapElements(array, j, j + 1)
            pIndex--
            i--
        }

        quickSort(array, left, pIndex - 1, transform)
        quickSort(array, pIndex + 1, right, transform)
    }

    /**
     * Swaps two elements in given array by their indices.
     */
    private fun swapElements(array: Array<String>, a: Int, b: Int) {
        val key = array[a]
        array[a] = array[b]
        array[b] = key
    }
}