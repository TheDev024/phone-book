import kotlin.math.floor
import kotlin.math.min
import kotlin.math.sqrt

/**
 * Searches for given elements in given arrays with help of linear and jump searching algorithms.
 */
object Searcher {

    fun binarySearch(array: Array<String>, element: String, left: Int = 0, right: Int = array.lastIndex, transform: (String) -> String): String? {
        if (right <= left) return null
        val middle = left + (right - left) / 2
        val middleElement = transform(array[middle])
        return if (array[middle].contains(element)) array[middle]
        else if (middleElement < element) binarySearch(array, element, middle + 1, right, transform)
        else binarySearch(array, element, left, middle - 1, transform)
    }

    /**
     * Searches for given element in given array with help of jump searching algorithm according to given transform
     * @param array array to search element in
     * @param element element to search in array
     * @param transform transform to apply to each element in array
     * @return if element found the element itself else null
     */
    fun jumpSearch(array: Array<String>, element: String, transform: (String) -> String): String? {
        if (array.isEmpty()) return null

        var current = 0
        var previous = 0
        val last = array.lastIndex
        val step = floor(sqrt(last.toDouble())).toInt()

        while (transform(array[current]) < element) {
            if (current == last) return null
            previous = current
            current = min(current + step, last)
        }

        while (transform(array[current]) > element) {
            current--
            if (current <= previous) return null
        }

        return if (array[current].contains(element)) array[current] else null
    }

    /**
     * Searches for given element in given array with help of linear searching algorithm according to given transform
     * @param array array to search element in
     * @param element element to search in array
     * @return if element found the element itself else null
     */
    fun linearSearch(array: Array<String>, element: String): String? {
        array.forEach {
            if (it.contains(element)) return it
        }
        return null
    }
}