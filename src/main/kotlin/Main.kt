import java.io.File

fun Long.formatDuration(): String {
    var duration = this
    val millis = duration % 1000
    duration = (duration - duration % 1000) / 1000
    val seconds = duration % 60
    val minutes = (duration - duration % 60) / 60
    return "$minutes min. $seconds sec. $millis ms."
}

fun main() {
    // declare paths
    val directoryPath = "directory.txt"
    val findPath = "find.txt"

    // declare files
    val directoryFile = File(directoryPath)
    val findFile = File(findPath)

    // initialize contacts
    val directoryContacts = directoryFile.readLines().toTypedArray()
    val findContacts = findFile.readLines().toTypedArray()

    println("Start searching (linear search)...")

    // start linear search timer
    val linearStart = System.currentTimeMillis()

    val count = findContacts.size
    var found = 0

    // start searching with linear search
    findContacts.forEach { find ->
        if (Searcher.linearSearch(directoryContacts, find) != null) found++
    }

    // stop linear search timer
    val linearStop = System.currentTimeMillis()

    val linearDuration = linearStop - linearStart

    println("Found $found / $count entries. Time taken: ${linearDuration.formatDuration()}\n")
    println("Start searching (bubble sort + jump search)...")

    // start sorting timer
    val bubbleStart = System.currentTimeMillis()

    // sort directory contacts
    Sorter.bubbleSort(directoryContacts) {
        it.replaceFirst("\\d+\\s*".toRegex(), "")
    }

    // stop sorting timer
    val bubbleStop = System.currentTimeMillis()

    val bubbleDuration = bubbleStop - bubbleStart

    // start jump search timer
    val jumpStart = System.currentTimeMillis()

    // start searching with jump search
    findContacts.forEach { find ->
        Searcher.jumpSearch(directoryContacts, find) {
            it.replaceFirst("\\d+\\s*".toRegex(), "")
        }
    }

    // stop jump search timer
    val jumpStop = System.currentTimeMillis()

    val jumpDuration = jumpStop - jumpStart
    var totalDuration = jumpDuration + bubbleDuration
    println(
        "Found $found / $count entries. Time taken: ${totalDuration.formatDuration()}\nSorting time: ${bubbleDuration.formatDuration()}\nSearching time: ${jumpDuration.formatDuration()}.\n"
    )
    println("Start searching (quick sort + binary search)...")

    // start sorting timer
    val quickStart = System.currentTimeMillis()

    // sort directory contacts
    Sorter.quickSort(directoryContacts) {
        it.replaceFirst("\\d+\\s*".toRegex(), "")
    }

    // stop sorting timer
    val quickStop = System.currentTimeMillis()

    val quickDuration = quickStop - quickStart

    // start jump search timer
    val binaryStart = System.currentTimeMillis()

    // start searching with jump search
    findContacts.forEach { find ->
        Searcher.binarySearch(directoryContacts, find) {
            it.replaceFirst("\\d+\\s*".toRegex(), "")
        }
    }

    // stop jump search timer
    val binaryStop = System.currentTimeMillis()

    val binaryDuration = binaryStop - binaryStart
    totalDuration = binaryDuration + quickDuration
    println(
        "Found $found / $count entries. Time taken: ${totalDuration.formatDuration()}\nSorting time: ${quickDuration.formatDuration()}\nSearching time: ${binaryDuration.formatDuration()}.\n"
    )

    println("Start searching (hash table)...")

    val hashCreateStart = System.currentTimeMillis()

    val hashContacts = directoryContacts.toHashSet()

    val hashCreateStop = System.currentTimeMillis()

    val hashCreateDuration = hashCreateStop - hashCreateStart

    val hashSearchStart = System.currentTimeMillis()

    findContacts.forEach { find ->
        hashContacts.find { it.contains(find) }
    }

    val hashSearchStop = System.currentTimeMillis()

    val hashSearchDuration = hashSearchStop - hashSearchStart

    totalDuration = hashCreateDuration + hashSearchDuration

    println("Found $found / $count entries. Time taken: ${totalDuration.formatDuration()}.\n" +
            "Creating time: ${hashCreateDuration.formatDuration()}.\n" +
            "Searching time: ${hashSearchDuration.formatDuration()}.")
}
